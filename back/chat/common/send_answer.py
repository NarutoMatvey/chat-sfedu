import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from chat.models import ExpectAnswer, Message, User


def main(question) -> None:
    massages, emails = get_data_to_send_answer(question)
    text = question.answer.answer
    send_answer_to_chat(text, massages)
    send_answer_to_mail(text, emails)


def get_data_to_send_answer(question) -> tuple:
    expect_answers = ExpectAnswer.objects.filter(question=question).values_list('message_id', 'email')

    massages, emails = [], []
    for index, data in expect_answers.iterator():
        if data[1] is not None:
            emails.append(data[1])
        else:
            massages.append(data[0])
    return massages, emails


def send_answer_to_chat(text: str, massages: list) -> None:
    Message.objects.filter(id__in=massages).update(message=text)


def send_answer_to_mail(text: str, emails: list) -> None:
    user = User.objects.get(username='admin')
    msg, password = create_msg_object(text, user)
    send_to_email(msg, password, emails)


def create_msg_object(text: str, user: User) -> tuple:
    msg = MIMEMultipart()
    msg['From'] = user.email
    password = user.email_password

    msg['Subject'] = "Subscription"
    msg.attach(MIMEText(text, 'plain'))
    return msg, password


def send_to_email(msg: MIMEMultipart, password: str, emails: list) -> None:
    server = smtplib.SMTP('smtp.gmail.com: 587')
    server.starttls()
    server.login(msg['From'], password)

    for email in emails:
        msg['To'] = email
        server.sendmail(msg['From'], msg['To'], msg.as_string())

    server.quit()
