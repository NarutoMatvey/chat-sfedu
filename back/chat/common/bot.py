from django.db.models import F, QuerySet

from chat.common import shingle
from chat.common.check_duplicate import check_duplicate
from chat.common.text_processing_with_nltk import preprocess_text
from chat.models import Question, Answer, Message


def bot_response_generation(message_pk: int, message_question: str) -> None:
    questions = search_question(message_question)
    check_and_create_question(questions, message_question)
    answers = get_answer(questions)
    create_message_answer(message_pk, answers)


def search_question(question_text: str) -> QuerySet:
    question_words = preprocess_text(question_text)
    shingle_vector = shingle.main(question_words)

    questions = check_duplicate(shingle_vector)
    questions.update(requests_number=F('requests_number') + 1)
    return questions


def check_and_create_question(questions: QuerySet, question: str) -> None:
    if not questions:
        Question.objects.create(question=question)


def get_answer(questions: QuerySet) -> QuerySet:
    if questions:
        answers = Answer.objects.filter(pk__in=questions.values_list('answer_id', flat=True))
    else:
        answers = Answer.objects.filter(pk=1)
    return answers


def create_message_answer(message_pk: int, answers: QuerySet) -> None:
    message = '\n'.join(answers.values_list('answer', flat=True))
    Message.objects.filter(pk=message_pk).update(message=message, generated=False)
