# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings

from chat.models import User


class SpecialQuerysetMixin(object):

    def perform_authentication(self, request):
        if request.user.is_authenticated:
            request.user = User.objects.get(pk=request.user.pk)
        else:
            request.user = User.objects.get(username='guest')
