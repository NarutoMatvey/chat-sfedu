import string

import nltk
from nltk.corpus import stopwords


def preprocess_text(text: str) -> list:
    text_without_punctuation = remove_punctuation(text.lower())
    tokenize_texts = tokenize_text(text_without_punctuation)
    text_without_stopwords = remove_stopwords(tokenize_texts, language='russian')
    stem_words(text_without_stopwords, language='russian')
    return text_without_stopwords


def remove_punctuation(text: str) -> str:
    translator = str.maketrans('', '', string.punctuation)
    return text.translate(translator)


def tokenize_text(text: str) -> list:
    return nltk.word_tokenize(text)


def remove_stopwords(words: list, language='russian') -> list:
    try:
        return [word for word in words if (word not in stopwords.words(language))]
    except:
        return words


def stem_words(words, language='russian'):
    stemmer = nltk.SnowballStemmer(language)
    return [stemmer.stem(word) for word in words]
