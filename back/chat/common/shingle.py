from django.db.models import BigIntegerField, Q, Count, QuerySet

from chat.common.crc64 import crc64


def main(words: list) -> list:
    shingles = generate_shingles(words)
    super_shingles = generate_super_shingle(shingles)
    return super_shingles


def generate_shingles(words: list, shingle_len=10) -> list:
    shingles = []
    for i in range(len(words) - (shingle_len - 1)):
        shingles.append(' '.join([x for x in words[i:i + shingle_len]]).encode('utf-8'))

    if len(shingles) == 0:
        shingles = [' '.join(words).encode('utf-8')]

    return shingles


def generate_super_shingle(shingles: list, shingle_len=100) -> list:
    super_shingle = []
    salt = ''

    for i in range(shingle_len):
        super_shingle.append(min([crc64(shingle + salt) - BigIntegerField.MAX_BIGINT for shingle in shingles]))
        salt += 'a'
    return super_shingle
