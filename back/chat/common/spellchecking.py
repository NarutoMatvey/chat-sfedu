import requests
import json

url = "https://speller.yandex.net/services/spellservice.json/checkText"


def spellchecking(text):
    params = {
        'lang': 'ru',
        'options': 6
    }

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    data = {'text': text}
    response = requests.post(url, headers=headers, params=params, data=data)

    json_response = response.json()
    result = json.dumps(json_response, indent=4)