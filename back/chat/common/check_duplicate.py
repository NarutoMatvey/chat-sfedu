from django.db.models import QuerySet, Q, Count

from chat.models import ShingleVectorQuestion


def check_duplicate(shingles: list) -> QuerySet:
    query = Q()
    for i, v in enumerate(shingles):
        query |= Q(position=i) & Q(hash=v)

    questions = ShingleVectorQuestion.objects.filter(query) \
        .values('question') \
        .annotate(matches=Count('*')) \
        .filter(matches__gt=10) \
        .values_list('question_id', flat=True)
    return questions
