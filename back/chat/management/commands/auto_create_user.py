#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand

from chat.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        # Пользователь Гость
        if User.objects.filter(username='guest').count() == 0:
            User.objects.create_user('guest', 'guest@mail.ru', 'guest')

        # Пользователь Админ
        if User.objects.filter(username='admin').count() == 0:
            User.objects.create_superuser('admin', 'admin@mail.ru', 'admin')

        # Пользователь Студент
        if User.objects.filter(username='student').count() == 0:
            User.objects.create_user('student', 'student@mail.ru', 'student')
