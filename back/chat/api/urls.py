from django.urls import path, include

urlpatterns = [
    path('answer/', include('chat.api.answer.urls')),
    path('category/', include('chat.api.category.urls')),
    path('message/', include('chat.api.message.urls')),
    path('question/', include('chat.api.question.urls')),
    path('user/', include('chat.api.user.urls'))
]