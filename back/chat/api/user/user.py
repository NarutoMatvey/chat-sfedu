from django_filters.rest_framework import FilterSet, DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from chat.api.user.serializer import UserSerializer
from chat.common.chat_permission import IsNotAuthenticated, IsNotAdmin
from chat.common.permission_mixed import MixedPermissionModelViewSet
from chat.models import User


class UserFilter(FilterSet):
    class Meta:
        model = User
        fields = ('age', 'last_name', 'first_name', 'patronymic', 'is_active')


class UserView(MixedPermissionModelViewSet):
    queryset = User.objects.exclude(username="guest")
    serializer_class = UserSerializer

    permission_classes_by_action = {
        'list': [IsAdminUser],
        'create': [IsNotAuthenticated],
        'login': [IsNotAuthenticated],
        'update': [IsAuthenticated],
        'retrieve': [IsAuthenticated],
        'destroy': [IsAuthenticated]
    }
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend, filters.SearchFilter]
    filter_class = UserFilter
    ordering_fields = {
        'last_name': 'last_name',
        'full_name': ['last_name', 'first_name', 'patronymic']
    }
    search_fields = ('last_name', 'first_name', 'patronymic')

    def perform_create(self, serializer):
        password = self.request.data.get('password')
        if password:
            user = serializer.save()
            user.set_password(password)
            user.save()

    def login(self, request, *args, **kwargs):
        username = request.data.get('username', None)
        password = request.data.get('password', None)

        user = self.get_queryset().filter(username=username).first()
        if user and user.check_password(password):
            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key}, status=status.HTTP_200_OK)
        return Response({'massage': 'Ошибка'}, status=status.HTTP_401_UNAUTHORIZED)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.id == request.user.id:
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        return Response({"detail": "You do not have permission to perform this action"},
                        status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if (instance.id == request.user.id or request.user.is_superuser) and not instance.is_superuser:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response({"detail": "You do not have permission to perform this action"},
                        status=status.HTTP_403_FORBIDDEN)