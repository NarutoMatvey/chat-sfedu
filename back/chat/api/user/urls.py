from django.urls import path
from chat.api.user.user import UserView

urlpatterns = [
    path('', UserView.as_view({'get': 'list', 'post': 'create'})),
    path('<int:pk>/', UserView.as_view({'get': 'retrieve', 'patch': 'update', 'delete': 'destroy'})),
    path('login/', UserView.as_view({'post': 'login'}))
]
