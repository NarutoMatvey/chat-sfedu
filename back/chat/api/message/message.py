import threading

from django_filters.rest_framework import FilterSet, DjangoFilterBackend
from rest_framework import viewsets, filters, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from chat.api.message.serializer import MessageSerializer
from chat.common.bot import bot_response_generation
from chat.common.special_mixin import SpecialQuerysetMixin
from chat.models import Message


class MessageFilter(FilterSet):
    class Meta:
        model = Message
        fields = ('message', 'side', 'generated', 'created', 'user')


class MessageView(SpecialQuerysetMixin, viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = [AllowAny]
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend, filters.SearchFilter]
    filter_class = MessageFilter
    ordering_fields = {
        'message': 'message',
        'side': 'side',
        'generated': 'generated',
        'created': 'created',
        'user': 'user_id'
    }
    search_fields = ('message',)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        massage_answer = self.perform_create(serializer)
        data = serializer.data
        data['message_answer'] = massage_answer
        headers = self.get_success_headers(serializer.data)

        return Response(data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        user = self.request.user
        message = serializer.save(user=user)
        massage_answer = Message.objects.create(message="Генерация ответа", side=False, generated=True, user=user)
        threading.Thread(target=bot_response_generation, args=[massage_answer.id, message.message]).start()
        return massage_answer.id

    def get_answer_message(self, request, *args, **kwargs):
        messages_id = request.query_params.get('messages', '').split(',')
        if messages_id:
            return Response(
                MessageSerializer(self.get_queryset().filter(pk__in=messages_id, generated=False), many=True).data
            )
        return Response([])
