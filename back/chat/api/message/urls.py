from django.urls import path

from chat.api.message.message import MessageView

urlpatterns = [
    path('get-answer-message/', MessageView.as_view({'get': 'get_answer_message'})),
    path('', MessageView.as_view({'get': 'list', 'post': 'create'})),
    path('<int:pk>/', MessageView.as_view({'get': 'retrieve', 'patch': 'update', 'delete': 'destroy'}))
]
