from rest_framework import serializers

from chat.models import Message


class MessageSerializer(serializers.ModelSerializer):
    side = serializers.ReadOnlyField()
    created = serializers.ReadOnlyField()

    class Meta:
        model = Message
        exclude = ('user', 'generated')
