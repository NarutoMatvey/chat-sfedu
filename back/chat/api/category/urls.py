from django.urls import path

from chat.api.category.category import CategoryView

urlpatterns = [
    path('', CategoryView.as_view({'get': 'list', 'post': 'create'})),
    path('<int:pk>/', CategoryView.as_view({'get': 'retrieve', 'patch': 'update', 'delete': 'destroy'}))
]
