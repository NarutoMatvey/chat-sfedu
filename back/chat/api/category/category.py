from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAdminUser

from chat.common.special_mixin import SpecialQuerysetMixin

from chat.api.category.serializer import CategorySerializer
from chat.models import Category


class CategoryFilter(FilterSet):
    class Meta:
        model = Category
        fields = ('name',)


class CategoryView(SpecialQuerysetMixin, viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    permission_classes = [IsAdminUser]

    filter_backends = [filters.OrderingFilter, DjangoFilterBackend, filters.SearchFilter]
    filter_class = CategoryFilter
    ordering_fields = {
        'name': 'name'
    }
    search_fields = ('name',)
