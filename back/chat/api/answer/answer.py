from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from rest_framework.permissions import IsAdminUser

from chat.common.special_mixin import SpecialQuerysetMixin
from rest_framework import filters
from rest_framework import viewsets

from chat.api.answer.serializer import AnswerSerializer
from chat.models import Answer


class AnswerFilter(FilterSet):
    class Meta:
        model = Answer
        fields = ('answer', 'category')


class AnswerView(SpecialQuerysetMixin, viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = [IsAdminUser]
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend, filters.SearchFilter]
    filter_class = AnswerFilter
    ordering_fields = {
        'answer': 'answer',
        'category': 'category_id'
    }
    search_fields = ('answer',)
