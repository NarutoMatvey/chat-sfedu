from django.urls import path

from chat.api.answer.answer import AnswerView

urlpatterns = [
    path('', AnswerView.as_view({'get': 'list', 'post': 'create'})),
    path('<int:pk>/', AnswerView.as_view({'get': 'retrieve', 'patch': 'update', 'delete': 'destroy'}))
]
