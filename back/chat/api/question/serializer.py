from rest_framework import serializers

from chat.models import Question


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        exclude = ('requests_number', )