from django_filters.rest_framework import FilterSet, DjangoFilterBackend
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAdminUser

import chat.common.send_answer as send_answer
from chat.api.question.serializer import QuestionSerializer
from chat.common.special_mixin import SpecialQuerysetMixin
from chat.models import Question


class QuestionFilter(FilterSet):
    class Meta:
        model = Question
        fields = ('question', 'requests_number', 'category')


class QuestionView(SpecialQuerysetMixin, viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [IsAdminUser]

    filter_backends = [filters.OrderingFilter, DjangoFilterBackend, filters.SearchFilter]
    filter_class = QuestionFilter
    ordering_fields = {
        'question': 'question',
        'requests_number': 'requests_number',
        'category': 'category_id',
        'category_name': 'category__name',
    }
    search_fields = ('question',)

    def perform_update(self, serializer):
        self.send_answer_to_user(serializer.save())

    def send_answer_to_user(self, question) -> None:
        if self.request.data.get('answer') != 1:
            send_answer.main(question)
