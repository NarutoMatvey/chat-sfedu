from django.urls import path

from chat.api.question.question import QuestionView

urlpatterns = [
    path('', QuestionView.as_view({'get': 'list', 'post': 'create'})),
    path('<int:pk>/', QuestionView.as_view({'get': 'retrieve', 'patch': 'partial_update', 'delete': 'destroy'}))
]
