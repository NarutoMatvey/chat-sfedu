from rest_framework import viewsets

from chat.api.expect_answer.serializer import ExpectAnswerSerializer
from chat.common.chat_permission import IsNotAdmin
from chat.common.special_mixin import SpecialQuerysetMixin
from chat.models import ExpectAnswer


class ExpectAnswerView(SpecialQuerysetMixin, viewsets.ModelViewSet):
    queryset = ExpectAnswer.objects.all()
    serializer_class = ExpectAnswerSerializer
    permission_classes = [IsNotAdmin]
