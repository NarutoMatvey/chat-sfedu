from django.urls import path
from chat.api.expect_answer.expect_answer import ExpectAnswerView

urlpatterns = [
    path('', ExpectAnswerView.as_view({'get': 'list', 'post': 'create'}))
]
