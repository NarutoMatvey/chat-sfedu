from rest_framework import serializers

from chat.models import ExpectAnswer


class ExpectAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpectAnswer
        exclude = ()
