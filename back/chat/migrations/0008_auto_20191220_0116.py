# Generated by Django 3.0 on 2019-12-19 22:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0007_expectanswer'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShingleVectorQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.IntegerField(default=0)),
                ('hash', models.BigIntegerField(default=0)),
                ('file', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chat.Question')),
            ],
        ),
        migrations.AddIndex(
            model_name='shinglevectorquestion',
            index=models.Index(fields=['position', 'hash'], name='chat_shingl_positio_2dc0b4_idx'),
        ),
    ]
