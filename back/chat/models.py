from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Index

from chat.common import shingle
from chat.common.text_processing_with_nltk import preprocess_text


class User(AbstractUser):
    patronymic = models.CharField(max_length=100, null=False, blank=True)
    email_password = models.CharField(max_length=100, null=True, blank=False)
    age = models.IntegerField(null=True,
                              validators=[MinValueValidator(1), MaxValueValidator(120)])

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.full_name()

    def full_name(self):
        return '{} {} {}'.format(self.last_name or '', self.first_name or '', self.patronymic or '').strip(' ')


class Message(models.Model):
    message = models.TextField(null=False, blank=False)
    side = models.BooleanField(default=True, null=False)
    generated = models.BooleanField(default=False, null=False)
    created = models.DateTimeField(auto_now_add=True, null=False)
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='messages', null=False)

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return self.message


class Category(models.Model):
    name = models.CharField(max_length=100, null=False)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Question(models.Model):
    question = models.CharField(max_length=300, null=False)
    requests_number = models.IntegerField(default=1)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, related_name='questions', default=1)
    answer = models.ForeignKey('Answer', on_delete=models.CASCADE, related_name='questions', default=1)

    def save(self, *args, **kwargs):
        question_words = preprocess_text(self.question)
        shingle_vector = shingle.main(question_words)
        for i, v in enumerate(shingle_vector):
            ShingleVectorQuestion.objects.update_or_create(position=i, question=self, defaults={'hash': v})
        super(Question, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'

    def __str__(self):
        return self.question


class Answer(models.Model):
    answer = models.TextField(null=False, blank=False)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, related_name='answers')

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'

    def __str__(self):
        return self.answer


class ExpectAnswer(models.Model):
    email = models.EmailField(blank=False, null=True)
    message = models.OneToOneField('Message', on_delete=models.CASCADE, null=False, related_name='+')
    question = models.ForeignKey('Question', null=False, on_delete=models.CASCADE, related_name='+')

    class Meta:
        verbose_name = 'Ожидает Ответ'
        verbose_name_plural = 'Ожидают Ответы'

    def __str__(self):
        return '{}:\n{}'.format(self.email, self.question.question)


class ShingleVectorQuestion(models.Model):
    position = models.IntegerField(null=False, default=0)
    hash = models.BigIntegerField(null=False, default=0)
    question = models.ForeignKey('Question', null=False, on_delete=models.CASCADE)

    class Meta:
        indexes = [
            Index(fields=['position', 'hash'])
        ]
